﻿using System;
using System.Text;
using Org.BouncyCastle.Security;

namespace Iodynis.Libraries.Encryption
{
    /// <summary>
    /// Secure random generator.
    /// </summary>
    public static class Random
    {
        private static readonly SecureRandom SecureRandom;
        static Random()
        {
            SecureRandom = new SecureRandom();
        }
        /// <summary>
        /// Generate a random ASCII string of the specified length. ASCII codes used are [32, 126].
        /// </summary>
        /// <param name="length">The length of the string.</param>
        /// <returns>A random ASCII string of the specified length.</returns>
        public static string String(int length)
        {
            char[] chars = new char[length];
            for (int charIndex = 0; charIndex < chars.Length; charIndex++)
            {
                byte @byte;
                while (true)
                {
                    @byte = SecureRandom.GetNextBytes(SecureRandom, 1)[0];
                    if (@byte > 31 /* the first 32 codes stand for non-printable symbols */ && @byte < 127 /* 127 code stands for DEL symbol, 128+ codes stand for symbols that depend on the used encoding */)
                    {
                        break;
                    }
                }
                chars[charIndex] = (char)@byte;
            }

            return new String(chars);
        }
        /// <summary>
        /// Generate a random byte array of the specified length.
        /// </summary>
        /// <param name="length">The length of the byte sequence.</param>
        /// <returns>A random array of the specified length.</returns>
        public static byte[] Bytes(int length)
        {
            // Get random and return
            byte[] bytes = new byte[length];
            Randomize(bytes);
            return bytes;
        }
        /// <summary>
        /// Fill the provided bytes buffer with random bytes.
        /// </summary>
        /// <param name="bytes">The buffer to fill with random bytes.</param>
        public static void Randomize(byte[] bytes)
        {
            SecureRandom.NextBytes(bytes);
        }
        /// <summary>
        /// Partially fill the provided bytes buffer with random bytes.
        /// </summary>
        /// <param name="bytes">The buffer to fill with random bytes.</param>
        /// <param name="index">The index of the first byte to write to.</param>
        /// <param name="length">The count of random bytes to write.</param>
        public static void Randomize(byte[] bytes, int index, int length)
        {
            SecureRandom.NextBytes(bytes, index, length);
        }
    }
}
