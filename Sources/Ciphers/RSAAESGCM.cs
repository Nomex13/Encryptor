﻿using System.Text;

namespace Iodynis.Libraries.Encryption
{
    /// <summary>
    /// RSA then AESGCM encryption. Key is encrypted with RSA and then the data is encrypted with AESGCM using that key.
    /// </summary>
    public static class RSAAESGCM
    {
        /// <summary>
        /// Encrypt.
        /// </summary>
        /// <param name="data">Plaintext data.</param>
        /// <param name="key">Private key in PEM format.</param>
        /// <returns>Encrypted data.</returns>
        public static byte[] Encrypt(byte[] data, byte[] key)
        {
            byte[] keyRsa = key;
            byte[] keyAes = AESGCM.GenerateKey();
            byte[] keyAesEncrypted = RSA.Encrypt(keyAes, keyRsa);
            byte[] dataEncrypted = AESGCM.Encrypt(data, keyAes, keyAesEncrypted);
            return dataEncrypted;
        }

        /// <summary>
        /// Decrypt.
        /// </summary>
        /// <param name="data">Encrypted data.</param>
        /// <param name="key">Public key in PEM format.</param>
        /// <returns>Plaintext data.</returns>
        public static byte[] Decrypt(byte[] data, byte[] key)
        {
            byte[] keyRsa = key;
            byte[] keyAesEncrypted = AESGCM.GetPlaintextPayload(data, AESGCM.KeyLength);
            byte[] keyAes = RSA.Decrypt(keyAesEncrypted, keyRsa);
            byte[] dataEncrypted = AESGCM.Decrypt(data, keyAes);
            return dataEncrypted;
        }

        /// <summary>
        /// Encrypt.
        /// </summary>
        /// <param name="data">Plaintext data.</param>
        /// <param name="key">Private key in PEM format.</param>
        /// <returns>Encrypted data.</returns>
        public static byte[] Encrypt(byte[] data, string key)
        {
            byte[] keyBytes = Encoding.ASCII.GetBytes(key);
            return Encrypt(data, keyBytes);
        }
        /// <summary>
        /// Decrypt.
        /// </summary>
        /// <param name="data">Encrypted data.</param>
        /// <param name="key">Public key in PEM format.</param>
        /// <returns>Plaintext data.</returns>
        public static byte[] Decrypt(byte[] data, string key)
        {
            byte[] keyBytes = Encoding.ASCII.GetBytes(key);
            return Decrypt(data, keyBytes);
        }
    }
}
