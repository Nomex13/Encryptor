# Encryptor

A wrapper around http://www.bouncycastle.org/csharp/ library.

## Usage

```csharp
string encrypted = AESThenHMAC.SimpleEncryptWithPassword(plaintext, password);
string decrypted = AESThenHMAC.SimpleDecryptWithPassword(encrypted, password);
```

## ToDo

Elliptic encryption is not implemented.

## License

Library is available under the MIT license.

Code in `AESThenHMAC` and `AESGCM` was posted by James Tuley at https://gist.github.com/4336842 and is free of known copyright restrictions under http://creativecommons.org/publicdomain/mark/1.0/ .

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.