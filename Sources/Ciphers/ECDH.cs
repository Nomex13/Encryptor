﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;

namespace Iodynis.Libraries.Encryption
{
    /// <summary>
    /// Elliptic encryption using the secp521r1 curve.
    /// </summary>
    public class ECDH
    {
        private const string Algorithm = "secp521r1";

        /// <summary>
        /// Encrypt.
        /// </summary>
        /// <param name="data">Plaintext data.</param>
        /// <param name="key">Private key in PEM format.</param>
        /// <returns>Encrypted data.</returns>
        public static byte[] Encrypt(byte[] data, byte[] key)
        {
            //RsaEngine rsaEngine = new RsaEngine();
            ECKeyParameters keyPrivate = PemToEcKeyParameters(key);
            //rsaEngine.Init(true, keyPrivate);
            ECDiffieHellman ecDiffieHellman = ECDiffieHellman.Create(Algorithm);


            //byte[] bytes = rsaEngine.ProcessBlock(param_data, 0, param_data.Length);

            //return bytes;
            throw new NotImplementedException();
        }

        /// <summary>
        /// Decrypt.
        /// </summary>
        /// <param name="data">Encrypted data.</param>
        /// <param name="key">Public key in PEM format.</param>
        /// <returns>Plaintext data.</returns>
        public static byte[] Decrypt(byte[] data, byte[] key)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Encrypt.
        /// </summary>
        /// <param name="data">Plaintext data.</param>
        /// <param name="key">Private key in PEM format.</param>
        /// <returns>Encrypted data.</returns>
        public static byte[] Encrypt(byte[] data, string key)
        {
            byte[] keyBytes = Encoding.ASCII.GetBytes(key);
            return Encrypt(data, keyBytes);
        }
        /// <summary>
        /// Decrypt.
        /// </summary>
        /// <param name="data">Encrypted data.</param>
        /// <param name="key">Public key in PEM format.</param>
        /// <returns>Plaintext data.</returns>
        public static byte[] Decrypt(byte[] data, string key)
        {
            byte[] keyBytes = Encoding.ASCII.GetBytes(key);
            return Decrypt(data, keyBytes);
        }

        /// <summary>
        /// Generate a pair of keys.
        /// </summary>
        /// <param name="keyPublic">The public key.</param>
        /// <param name="keyPrivate">The private key.</param>
        public void GenerateKeyPair(out byte[] keyPublic, out byte[] keyPrivate)
        {
            // Generate Diffie-Hellman parameters
            DHParametersGenerator dhParametersGenerator = new DHParametersGenerator();
            DHParameters dhParameters = dhParametersGenerator.GenerateParameters();
            DHKeyGenerationParameters dhKeyGenerationParameters = new DHKeyGenerationParameters(new SecureRandom(), dhParameters);

            // Init key generator
            ECKeyPairGenerator ecKeyPairGenerator = new ECKeyPairGenerator(Algorithm);
            ecKeyPairGenerator.Init(dhKeyGenerationParameters);

            // Generate keys
            AsymmetricCipherKeyPair pair = ecKeyPairGenerator.GenerateKeyPair();
            AsymmetricKeyParameter keyParameterPrivate = pair.Private;
            AsymmetricKeyParameter keyParameterPublic  = pair.Public;

            // Convert both keys to PEM format
            PemWriter pemWriter = new PemWriter(new StringWriter());
            pemWriter.WriteObject(keyParameterPrivate);
            pemWriter.Writer.Flush();
            keyPrivate = Encoding.ASCII.GetBytes(pemWriter.Writer.ToString());
            pemWriter.WriteObject(keyParameterPublic);
            pemWriter.Writer.Flush();
            keyPublic = Encoding.ASCII.GetBytes(pemWriter.Writer.ToString());
        }

        private static ECKeyParameters PemToEcKeyParameters(byte[] pem)
        {
            ECKeyParameters ecKeyParameters;
            using (MemoryStream memoryStream = new MemoryStream(pem))
            {
                using (StreamReader streamReader = new StreamReader(memoryStream))
                {
                    PemReader pemReader = new PemReader(streamReader);
                    ecKeyParameters = (ECKeyParameters)pemReader.ReadObject();
                }
            }
            return ecKeyParameters;
        }
    }
}
