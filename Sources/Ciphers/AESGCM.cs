﻿/*
 * This work (Modern Encryption of a String C#, by James Tuley),
 * identified by James Tuley, is free of known copyright restrictions.
 * https://gist.github.com/4336842
 * http://creativecommons.org/publicdomain/mark/1.0/
 */

using System;
using System.IO;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace Iodynis.Libraries.Encryption
{
    /// <summary>
    /// AES with GSM encryption.
    /// </summary>
    public static class AESGCM
    {
        private static readonly SecureRandom Random = new SecureRandom();

        //Preconfigured Encryption Parameters
        private const int NonceBitSize = 128;
        private const int MacBitSize = 128;
        private const int KeyBitSize = 256;

        //Preconfigured Password Key Derivation Parameters
        private const int SaltBitSize = 128;
        private const int Iterations = 10000;
        private const int MinPasswordLength = 2;
        /// <summary>
        /// The key length in bytes.
        /// </summary>
        public static int KeyLength
        {
            get
            {
                return KeyBitSize / 8;
            }
        }
        /// <summary>
        /// Helper that generates a random new key on each call.
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateKey()
        {
            byte[] key = new byte[KeyLength];
            Random.NextBytes(key);
            return key;
        }

        /// <summary>
        /// Simple Encryption And Authentication (AES-GCM) of a UTF8 string.
        /// </summary>
        /// <param name="string">The secret message.</param>
        /// <param name="key">The key.</param>
        /// <param name="plaintextPayload">Optional non-secret payload.</param>
        /// <returns>Encrypted message.</returns>
        /// <exception cref="System.ArgumentException">Secret Message Required!;secretMessage</exception>
        /// <remarks>Adds overhead of (Optional-Payload + BlockSize(16) + Message +  HMac-Tag(16)) * 1.33 Base64.</remarks>
        public static string Encrypt(string @string, byte[] key, byte[] plaintextPayload)
        {
            if (String.IsNullOrEmpty(@string))
            {
                throw new ArgumentException("Text to encrypt is required.", nameof(@string));
            }

            byte[] plainText = Encoding.UTF8.GetBytes(@string);
            byte[] cipherText = Encrypt(plainText, key, plaintextPayload);
            return Convert.ToBase64String(cipherText);
        }


        /// <summary>
        /// Simple Decryption &amp; Authentication (AES-GCM) of a UTF8 Message.
        /// </summary>
        /// <param name="string">The encrypted message.</param>
        /// <param name="key">The key.</param>
        /// <param name="plaintextPayload">Optional non-secret payload.</param>
        /// <param name="plaintextPayloadLength">Length of the optional non-secret payload.</param>
        /// <returns>Decrypted message.</returns>
        public static string Decrypt(string @string, byte[] key, out byte[] plaintextPayload, int plaintextPayloadLength = 0)
        {
            if (String.IsNullOrEmpty(@string))
            {
                throw new ArgumentException("Text to decrypt is required.", nameof(@string));
            }

            byte[] cipherText = Convert.FromBase64String(@string);
            byte[] plaintext = Decrypt(cipherText, key, out plaintextPayload, plaintextPayloadLength);
            return plaintext == null ? null : Encoding.UTF8.GetString(plaintext);
        }

        /// <summary>
        /// Simple Encryption And Authentication (AES-GCM) of a UTF8 String using key derived from a password (PBKDF2).
        /// </summary>
        /// <param name="string">The secret message.</param>
        /// <param name="password">The password.</param>
        /// <param name="plaintextPayload">The non secret payload.</param>
        /// <returns>Encrypted message.</returns>
        /// <remarks>Significantly less secure than using random binary keys. Adds additional non secret payload for key generation parameters.</remarks>
        public static string Encrypt(string @string, string password, byte[] plaintextPayload)
        {
            if (String.IsNullOrEmpty(@string))
            {
                throw new ArgumentException("Text to encrypt is required.", nameof(@string));
            }

            byte[] plainText = Encoding.UTF8.GetBytes(@string);
            byte[] cipherText = Encrypt(plainText, password, plaintextPayload);
            return Convert.ToBase64String(cipherText);
        }


        /// <summary>
        /// Simple Decryption and Authentication (AES-GCM) of a UTF8 message using a key derived from a password (PBKDF2).
        /// </summary>
        /// <param name="string">The encrypted message.</param>
        /// <param name="password">The password.</param>
        /// <param name="plaintextPayload">Optional non-secret payload.</param>
        /// <param name="plaintextPayloadLength">Length of the non secret payload.</param>
        /// <returns>Decrypted message.</returns>
        /// <exception cref="System.ArgumentException">Encrypted Message Required!;encryptedMessage</exception>
        /// <remarks>Significantly less secure than using random binary keys.</remarks>
        public static string Decrypt(string @string, string password, out byte[] plaintextPayload, int plaintextPayloadLength = 0)
        {
            if (String.IsNullOrWhiteSpace(@string))
            {
                throw new ArgumentException("Text to decrypt is required.", nameof(@string));
            }

            byte[] cipherText = Convert.FromBase64String(@string);
            byte[] plaintext = Decrypt(cipherText, password, out plaintextPayload, plaintextPayloadLength);
            return plaintext == null ? null : Encoding.UTF8.GetString(plaintext);
        }


        /// <summary>
        /// Simple Encryption And Authentication (AES-GCM) of a UTF8 string.
        /// </summary>
        /// <param name="bytes">The secret message.</param>
        /// <param name="key">The key.</param>
        /// <param name="plaintextPayload">Optional non-secret payload.</param>
        /// <returns>Encrypted message.</returns>
        /// <remarks>Adds overhead of (Optional-Payload + BlockSize(16) + Message +  HMac-Tag(16)) * 1.33 Base64.</remarks>
        public static byte[] Encrypt(byte[] bytes, byte[] key, byte[] plaintextPayload)
        {
            //User Error Checks
            if (key == null || key.Length != KeyBitSize / 8)
            {
                throw new ArgumentException(String.Format("Key needs to be {0} bit!", KeyBitSize), "param_key");
            }
            if (bytes == null || bytes.Length == 0)
            {
                throw new ArgumentException("Bytes to encrypt are required.", nameof(bytes));
            }

            //Non-secret Payload Optional
            plaintextPayload = plaintextPayload ?? new byte[] { };

            //Using random nonce large enough not to repeat
            byte[] nonce = new byte[NonceBitSize / 8];
            Random.NextBytes(nonce, 0, nonce.Length);

            GcmBlockCipher cipher = new GcmBlockCipher(new AesEngine());
            AeadParameters parameters = new AeadParameters(new KeyParameter(key), MacBitSize, nonce, plaintextPayload);
            cipher.Init(true, parameters);

            //Generate Cipher Text With Auth Tag
            byte[] cipherText = new byte[cipher.GetOutputSize(bytes.Length)];
            int len = cipher.ProcessBytes(bytes, 0, bytes.Length, cipherText, 0);
            cipher.DoFinal(cipherText, len);

            //Assemble Message
            using (var combinedStream = new MemoryStream())
            {
                using (var binaryWriter = new BinaryWriter(combinedStream))
                {
                    //Prepend Authenticated Payload
                    binaryWriter.Write(plaintextPayload);
                    //Prepend Nonce
                    binaryWriter.Write(nonce);
                    //Write Cipher Text
                    binaryWriter.Write(cipherText);
                }
                return combinedStream.ToArray();
            }
        }

        /// <summary>
        /// Simple Decryption &amp; Authentication (AES-GCM) of a UTF8 Message.
        /// </summary>
        /// <param name="bytes">The encrypted message.</param>
        /// <param name="key">The key.</param>
        /// <param name="plaintextPayload">Optional non-secret payload.</param>
        /// <param name="plaintextPayloadLength">Length of the optional non-secret payload.</param>
        /// <returns>Decrypted message.</returns>
        public static byte[] Decrypt(byte[] bytes, byte[] key, out byte[] plaintextPayload, int plaintextPayloadLength = 0)
        {
            //User Error Checks
            if (key == null || key.Length != KeyBitSize / 8)
            {
                throw new ArgumentException(String.Format("Key needs to be {0} bit!", KeyBitSize), "param_key");
            }
            if (bytes == null || bytes.Length == 0)
            {
                throw new ArgumentException("Bytes to decrypt are required.", nameof(bytes));
            }

            using (var cipherStream = new MemoryStream(bytes))
            using (var cipherReader = new BinaryReader(cipherStream))
            {
                //Grab Payload
                plaintextPayload = cipherReader.ReadBytes(plaintextPayloadLength);

                //Grab Nonce
                byte[] nonce = cipherReader.ReadBytes(NonceBitSize / 8);

                GcmBlockCipher cipher = new GcmBlockCipher(new AesEngine());
                AeadParameters parameters = new AeadParameters(new KeyParameter(key), MacBitSize, nonce, plaintextPayload);
                cipher.Init(false, parameters);

                //Decrypt Cipher Text
                byte[] cipherText = cipherReader.ReadBytes(bytes.Length - plaintextPayloadLength - nonce.Length);
                byte[] plainText = new byte[cipher.GetOutputSize(cipherText.Length)];

                try
                {
                    var len = cipher.ProcessBytes(cipherText, 0, cipherText.Length, plainText, 0);
                    cipher.DoFinal(plainText, len);

                    // TODO: check it (https://stackoverflow.com/questions/10396762/how-do-i-decrypt-aes-ccm-encrypted-cipher-text-with-bouncy-castle)
                    byte[] mac = cipher.GetMac();
                    for (int i = 0; i < mac.Length; i++)
                    {
                        if (mac[0] != cipherText[cipherText.Length - mac.Length + i])
                        {
                            throw new InvalidCipherTextException();
                        }
                    }
                }
                catch (InvalidCipherTextException)
                {
                    //Return null if it doesn't authenticate
                    return null;
                }

                return plainText;
            }

        }
        /// <summary>
        /// Get plaintext payload.
        /// </summary>
        /// <param name="bytes">Bytes.</param>
        /// <param name="plaintextPayloadLength">Length of the plaintext payload.</param>
        /// <returns></returns>
        public static byte[] GetPlaintextPayload(byte[] bytes, int plaintextPayloadLength = 0)
        {
            return bytes.Take(plaintextPayloadLength).ToArray();
        }

        /// <summary>
        /// Simple Encryption And Authentication (AES-GCM) of a UTF8 String using key derived from a password.
        /// </summary>
        /// <param name="bytes">The secret message.</param>
        /// <param name="password">The password.</param>
        /// <param name="plaintextPayload">The non secret payload.</param>
        /// <returns>Encrypted message.</returns>
        /// <exception cref="System.ArgumentException">Must have a password of minimum length;password</exception>
        /// <remarks>Significantly less secure than using random binary keys. Adds additional non secret payload for key generation parameters.</remarks>
        public static byte[] Encrypt(byte[] bytes, string password, byte[] plaintextPayload)
        {
            plaintextPayload = plaintextPayload ?? new byte[] { };

            //User Error Checks
            if (string.IsNullOrWhiteSpace(password) || password.Length < MinPasswordLength)
            {
                throw new ArgumentException(String.Format("Must have a password of at least {0} characters!", MinPasswordLength), "param_password");
            }
            if (bytes == null || bytes.Length == 0)
            {
                throw new ArgumentException("Secret Message Required!", "param_bytes");
            }

            Pkcs5S2ParametersGenerator generator = new Pkcs5S2ParametersGenerator();

            //Use Random Salt to minimize pre-generated weak password attacks.
            byte[] salt = new byte[SaltBitSize / 8];
            Random.NextBytes(salt);

            generator.Init(
                PbeParametersGenerator.Pkcs5PasswordToBytes(password.ToCharArray()),
                salt,
                Iterations);

            //Generate Key
            KeyParameter key = (KeyParameter)generator.GenerateDerivedMacParameters(KeyBitSize);

            //Create Full Non Secret Payload
            byte[] payload = new byte[salt.Length + plaintextPayload.Length];
            Array.Copy(plaintextPayload, payload, plaintextPayload.Length);
            Array.Copy(salt, 0, payload, plaintextPayload.Length, salt.Length);

            return Encrypt(bytes, key.GetKey(), payload);
        }

        /// <summary>
        /// Simple Decryption and Authentication of a UTF8 message using a key derived from a password.
        /// </summary>
        /// <param name="bytes">The encrypted message.</param>
        /// <param name="password">The password.</param>
        /// <param name="plaintextPayload">Optional non-secret payload.</param>
        /// <param name="plaintextPayloadLength">Length of the non secret payload.</param>
        /// <returns>Decrypted message.</returns>
        /// <exception cref="System.ArgumentException">Must have a password of minimum length;password</exception>
        /// <remarks>Significantly less secure than using random binary keys.</remarks>
        public static byte[] Decrypt(byte[] bytes, string password, out byte[] plaintextPayload, int plaintextPayloadLength = 0)
        {
            //User Error Checks
            if (string.IsNullOrWhiteSpace(password) || password.Length < MinPasswordLength)
            {
                throw new ArgumentException(String.Format("Must have a password of at least {0} characters!", MinPasswordLength), "param_password");
            }
            if (bytes == null || bytes.Length == 0)
            {
                throw new ArgumentException("Encrypted Message Required!", "param_bytes");
            }

            Pkcs5S2ParametersGenerator generator = new Pkcs5S2ParametersGenerator();

            //Grab Salt from Payload
            byte[] salt = new byte[SaltBitSize / 8];
            Array.Copy(bytes, plaintextPayloadLength, salt, 0, salt.Length);

            generator.Init(
                PbeParametersGenerator.Pkcs5PasswordToBytes(password.ToCharArray()),
                salt,
                Iterations);

            //Generate Key
            KeyParameter key = (KeyParameter)generator.GenerateDerivedMacParameters(KeyBitSize);

            return Decrypt(bytes, key.GetKey(), out plaintextPayload, salt.Length + plaintextPayloadLength);
        }

        /// <summary>
        /// Simple Decryption &amp; Authentication (AES-GCM) of a UTF8 Message.
        /// </summary>
        /// <param name="string">The encrypted message.</param>
        /// <param name="password">The password.</param>
        /// <returns>Decrypted message.</returns>
        public static string Decrypt(string @string, string password)
        {
            return Decrypt(@string, password, out _);
        }

        /// <summary>
        /// Simple Decryption &amp; Authentication (AES-GCM) of a UTF8 Message.
        /// </summary>
        /// <param name="string">The encrypted message.</param>
        /// <param name="key">The key.</param>
        /// <returns>Decrypted message.</returns>
        public static string Decrypt(string @string, byte[] key)
        {
            return Decrypt(@string, key, out _);
        }

        /// <summary>
        /// Simple Decryption &amp; Authentication (AES-GCM) of a UTF8 Message.
        /// </summary>
        /// <param name="bytes">The encrypted message.</param>
        /// <param name="password">The password.</param>
        /// <returns>Decrypted message.</returns>
        public static byte[] Decrypt(byte[] bytes, string password)
        {
            return Decrypt(bytes, password, out _);
        }

        /// <summary>
        /// Simple Decryption &amp; Authentication (AES-GCM) of a UTF8 Message.
        /// </summary>
        /// <param name="bytes">The encrypted message.</param>
        /// <param name="key">The key.</param>
        /// <returns>Decrypted message.</returns>
        public static byte[] Decrypt(byte[] bytes, byte[] key)
        {
            return Decrypt(bytes, key, out _);
        }

    }
}